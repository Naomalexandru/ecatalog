package com.org.ecatalog.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;
@Entity
@Table(name = "school_class")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class SchoolClass {
@Id
@GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;
@Column(name = "starting_year")
    private Integer startingYear;

    private String name;
    @Column(name = "class_level")
    @Enumerated(value = EnumType.STRING)
    private ClassLevel classLevel;

}
