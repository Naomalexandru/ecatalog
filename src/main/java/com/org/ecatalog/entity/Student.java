package com.org.ecatalog.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Student {
@Id
@GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;
@Column(name = "first_name")
    private String firstName;
@Column(name = "last_name")
    private String lastName;
    @Column(unique = true)
    private String cnp;

    @Column(name = "class_id")
    private UUID classId;
    @ManyToOne(targetEntity = SchoolClass.class,fetch = FetchType.EAGER )
    @JoinColumn(name = "class_id",insertable = false,updatable = false)
    private SchoolClass schoolClass;
}
