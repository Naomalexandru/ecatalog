package com.org.ecatalog.dto;

import com.org.ecatalog.entity.ClassLevel;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class SchoolClassCreateDto {
    private Integer startingYear;

    private String name;

    private ClassLevel classLevel;
}
