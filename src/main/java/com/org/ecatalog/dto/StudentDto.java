package com.org.ecatalog.dto;

import com.org.ecatalog.entity.SchoolClass;
import lombok.*;

import java.util.UUID;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class StudentDto {
    private UUID id;
    private String firstName;
    private String lastName;
    private String cnp;
    private SchoolClassDto schoolClass;
}
