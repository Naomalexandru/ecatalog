package com.org.ecatalog.dto;

import lombok.*;

import java.util.UUID;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class StudentCreateDto {
    private String firstName;
    private String lastName;
    private String cnp;

}
