package com.org.ecatalog.mapper;

import com.org.ecatalog.dto.SchoolClassCreateDto;
import com.org.ecatalog.dto.SchoolClassDto;
import com.org.ecatalog.entity.SchoolClass;
import com.org.ecatalog.repository.SchoolClassRepository;
import org.springframework.stereotype.Component;

@Component
public class SchoolClassMapper {

    public SchoolClass toEntity(SchoolClassCreateDto createDto) {
        SchoolClass entity = SchoolClass.builder()
                .name(createDto.getName())
                .startingYear(createDto.getStartingYear())
                .classLevel(createDto.getClassLevel())
                .build();
        return entity;
    }

        public SchoolClassDto toDto (SchoolClass entity){
            SchoolClassDto schoolClassDto = SchoolClassDto.builder()
                    .id(entity.getId())
                    .classLevel(entity.getClassLevel())
                    .startingYear(entity.getStartingYear())
                    .name(entity.getName())
                    .build();
            return schoolClassDto;
        }
    }

