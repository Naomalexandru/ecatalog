package com.org.ecatalog.service;

import com.org.ecatalog.dto.SchoolClassCreateDto;
import com.org.ecatalog.dto.SchoolClassDto;
import com.org.ecatalog.entity.SchoolClass;
import com.org.ecatalog.exceptions.SchoolClassInvalidException;
import com.org.ecatalog.mapper.SchoolClassMapper;
import com.org.ecatalog.repository.SchoolClassRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class SchoolClassService {
    private final SchoolClassRepository schoolClassRepository;
    private final SchoolClassMapper schoolClassMapper;

    public List<SchoolClassDto> findAll() {

        return schoolClassRepository.findAll().stream().map(schoolClass -> {

            SchoolClassDto dto = schoolClassMapper.toDto(schoolClass);
                    return dto;
        }).collect(Collectors.toList());

    }

    public SchoolClassDto createClass(SchoolClassCreateDto schoolClassCreateDto) throws SchoolClassInvalidException {
       List<String > errors =validateSchoolClass(schoolClassCreateDto);
       if(errors.isEmpty() == false){
           throw new SchoolClassInvalidException(errors);
       }
        //transformare din schoolClassCreateDto in obiect schoolClass
        SchoolClass entity = schoolClassMapper.toEntity(schoolClassCreateDto);
        //salvarea obiectului schoolClass
        SchoolClass schoolClassWithId = schoolClassRepository.save(entity);
        //returnarea unui obiect de tipul schoolClassDto
        return schoolClassMapper.toDto(schoolClassWithId);
    }
    private List<String> validateSchoolClass(SchoolClassCreateDto dto){
        List<String> errors = new ArrayList<>();
        if(dto.getStartingYear()> LocalDate.now().getYear()){
            errors.add("year " + dto.getStartingYear() + " is invalid.The starting year should not be in the future");
        }
        return errors;
    }
}
